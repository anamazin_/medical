//public Calculator value variables
var ageGroup_calc;
var sex_calc;
var functionalStatus_calc;
var emergencyCase_calc;
var ASAClass_calc;
var steroidUse_calc;
var ascites_calc;
var systemicSepsis_calc;
var ventilatorDependent_calc;
var disseminatedCancer_calc;
var diabetes_calc;
var hypertension_calc;
var congestiveHeartFailure_calc;
var dyspnea_calc;
var currentSmoker_calc;
var severeCOPD_calc;
var dialysis_calc;
var renalFailure_calc;
var BMIHeight_calc;
var BMIWeight_calc;
var otherTreatmentOptions_calc;
var HGB_calc;
var albumium_calc;
var currentCigaretteUse_calc;
var independentFunctioning_calc;
var prescriptionSubstances_calc;

function getValuesFromCalculatorForm() {
	ageGroup_calc = document.getElementById("sel1").val();
	sex_calc = document.getElementById("sel2").val();
	functionalStatus_calc = document.getElementById("sel3").val();
	emergencyCase_calc = document.getElementById("sel4").val();
	ASAClass_calc = document.getElementById("sel5").val();
	steroidUse_calc = document.getElementById("sel6").val();
	ascites_calc = document.getElementById("sel7").val();
	systemicSepsis_calc = document.getElementById("sel8").val();
	ventilatorDependent_calc = document.getElementById("sel9").val();
	disseminatedCancer_calc = document.getElementById("sel10").val();
	diabetes_calc = document.getElementById("sel11").val();
	hypertension_calc = document.getElementById("sel12").val();
	congestiveHeartFailure_calc = document.getElementById("sel13").val();
	dyspnea_calc = document.getElementById("sel14").val();
	currentSmoker_calc = document.getElementById("sel15").val();
	severeCOPD_calc = document.getElementById("sel16").val();
	dialysis_calc = document.getElementById("sel17").val();
	renalFailure_calc = document.getElementById("sel18").val();
	//BMIHeight_calc = document.getElementById("sel19").val();
	//BMIWeight_calc = document.getElementById("sel20").val();
	//otherTreatmentOptions_calc = document.getElementById("sel21").val();
	HGB_calc = document.getElementById("sel20").val();
	albumium_calc = document.getElementById("sel21").val();
	currentCigaretteUse_calc = document.getElementById("sel22").val();
	independentFunctioning_calc = document.getElementById("sel23").val();
	prescriptionSubstances_calc = document.getElementById("sel24").val();
}

function assignValues() {
	var evaluation = {
		"ageGroup": ageGroup_calc,
	    "sex": sex_calc,
	    "functionalStatus": functionalStatus_calc,
	    "emergencyCase": emergencyCase_calc,
	    "ASAClass": ASAClass_calc,
	    "steroidUse": steroidUse_calc,
	    "ascites": ascites_calc,
	    "systemicSepsis": systemicSepsis_calc,
	    "ventilatorDependent": ventilatorDependent_calc,
	    "disseminatedCancer": disseminatedCancer_calc,
	    "diabetes": diabetes_calc,
	    "hypertension": hypertension_calc,
	    "congestiveHeartFailure": congestiveHeartFailure_calc,
	    "dyspnea": dyspnea_calc,
	    "currentSmoker": currentSmoker_calc,
	    "severeCOPD": severeCOPD_calc,
	    "dialysis": dialysis_calc,
	    "renalFailure": renalFailure_calc,
	    //"BMIHeight": BMIHeight_calc,
	    //"BMIWeight": BMIWeight_calc,
	    //"otherTreatmentOptions": otherTreatmentOptions_calc,
	    "HGB": HGB_calc,
	    "albumium": albumium_calc,
	    "currentCigaretteUse": currentCigaretteUse_calc,
	    "independentFunctioning": independentFunctioning_calc,
	    "prescriptionControlledSubstances": prescriptionSubstances_calc
	}
}

$.post( "http://0179713.upweb.site:3000", evaluation, function(error){
	if(error){
		alert("Error");
	}else{
		alert("Data saved succesfully");
	}
});

$.get();

function getMethod(key, what){
	url ="http://0179713.upweb.site:3000/api/medical_databases?filter=%7B%22where%22%3A%7B%22";
	url +=key; 
	url += "%22%3A%22";
	url += what;
	url += "%22%7D%7D";
	$.get(url, function(data){
		myInfo = data;
		alert(myInfo);
	})
}

//http://0179713.upweb.site:3000/explorer/#/